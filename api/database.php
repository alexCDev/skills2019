 <?php  

	require_once('vendor/autoload.php');
	require_once('encryption.php');

	class DB {

		private $response;
		private $db;

		public function __construct(Connection $db) {
			$this->db = $db;
		}

		public function getResponse() {
			return $this->response;
		}

		public function register($account = array()) {
			$username = $account['username'];
			$password = $account['password'];
			if(!empty($username) && !empty($password)) {
				$enc = new EncryptionHelper($password);
				$enc->protect();
				$ePassword = $enc->getResponse()['hashed'];
				$exists = $this->db->exe("SELECT * FROM users WHERE username=?", [$username])->fetch();
				if($exists['username'] == $username) {
					$this->response = array('error'=>'user_exists');
				} else {
					if($this->db->exe("INSERT INTO users (id, username, password, rights) VALUES (?, ?, ?, ?)", ['', $username, $ePassword, 0])) {
						$this->response = array('success'=>'account_created', 'password'=>$password);
					} else {
						$this->response = array('error'=>'account_creation_failed');
					}
				}
			} else {
				$this->response = array('error'=>'empty_data');
			}
		}

		public function login($account = array()) {
			$username = $account['username'];
			$password = $account['password'];
			if(!empty($username) && !empty($password)) {
				$enc = new EncryptionHelper($password);
				$enc->protect();
				$ePassword = $enc->getResponse()['hashed'];
				$check = $this->db->exe("SELECT * FROM users WHERE username=? AND password IS NOT NULL", [$username])->fetch();
				$enc->verify($password, $check['password']);
				if($check['username'] == $username && $enc->getResponse()['status'] == "matching") {
					switch(intval($check['rights'])) {
						case 0:
							$_SESSION['account'] = json_encode(array('username'=>$username, 'login_timestamp'=>time()));
						break;
						case 1:
							$_SESSION['account'] = json_encode(array('username'=>$username, 'login_timestamp'=>time(), 'admin_user'=>true, 'admin_name'=>$username));
						default:
							$_SESSION['account'] = json_encode(array('username'=>$username, 'login_timestamp'=>time()));
						break;
					}
					$this->response = array('success'=>'login_success');
				} else {
					$this->response = array('error'=>'login_error');
				}
			} else {
				$this->response = array('error'=>'empty_data');
			}
		}

		public function getPricing($products) {
			$subtotal = 0;
			$prod = json_decode($products);
			foreach($prod->ids as $id) {
				$dbProducts = $this->db->exe("SELECT * FROM products WHERE usable_id=?", [$id])->fetch();
				$pricing .= $dbPrudcts['price'];
				$subtotal = $pricing;
			}
			$this->response = array('subtotal'=>$subtotal);
		}

		public function checkout($cart = array()) {
			$keys = [
				"secret_key"=>'sk_test_4Nyd48SHpnhoqhvSCmVvPpyD',
				"publishable_key"=>'pk_test_4NydDpwAHwKaXCg9Dxgx8M2Z'
			];
			$products = $cart['products'];//all products sent from frontend cart
			$purchaser = $cart['purchaser'];
			$price = $cart['price'];
			//ability to use self?
			$orderId = self::genOrderId($purchaser);
			if(!empty($products) && !empty($purchaser) && !empty($orderId)) {
				$token = $cart['stripe']['token'];
				$email = $cart['stripe']['email'];
				\Stripe\Stripe::setApiKey($keys['secret_key']);
				$customer = \Stripe\Customer::create([
					"email"=>$email,
					"source"=>$token
				]);
				$charge = \Stripe\Charge::create([
					"customer"=>$customer->id,
					"amount"=>5000,
					"currency"=>"usd"
				]);
			} else {
				$this->response = array('error'=>'empty_data');
			}
		}

		public function genOrderId($purchaser) {
			$orderId = bin2hex(openssl_random_pseudo_bytes(15))."[{$purchaser}]";
			return $orderId;
		}

	}

?>