<?php 

	session_start();

	require_once('connection.php');
	require_once('database.php');

	if(isset($_POST)) {
		print_r(parseRequests($_POST));
	}

	function parseRequests($handler = array()) {
		$connection = new Connection("mysql:host=localhost;dbname=bakersdirect;charset=utf8", "root", "");
		$db = new DB($connection);
		switch($handler['action']) {
			case "login":
				$username = $handler['username'];
				$password = $handler['password'];
				$db->login(array(
					"username"=>$username,
					"password"=>$password
				));
				return json_encode($db->getResponse());
			break;
			case "register":
				$username = $handler['username'];
				$password = $handler['password'];
				$db->register(array(
					"username"=>$username,
					"password"=>$password
				));
				return json_encode($db->getResponse());
			break;
			case "checkout":
				$purchaser = json_decode($_SESSION['account'])->username;
				$products = $handler['products'];
				//stripe info
				$token = $handler['stripeToken'];
				$email = $handler['stripeEmail'];
				$db->checkout(array(
					"purchaser"=>$purchaser,
					"products"=>$products,
					"stripe"=>array(
						"token"=>$token,
						"email"=>$email
					)
				));
				return json_encode($db->getResponse());
			break;
			case "getpricing":
				$products = $handler['products'];
				$db->getPricing($products);
				return json_encode($db->getResponse());
			break;
			case "setcart":
				$cart = $handler['products'];
				$_SESSION['cart'] = $cart;
				return json_encode(array('success'=>'started_cart_session'));
			break;
			default:
				return json_encode(array('error'=>'invalid_action'));
			break;
		}
	}

?>