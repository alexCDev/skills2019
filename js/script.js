$(document).ready(function(){
  $("selectMuffin").hover(function(){
    $(this).css("background-color", "yellow");
    }, function(){
    $(this).css("background-color", "pink");
  });
});

var muffinsDialog = document.querySelector('#muffinsDialog');
var continueDialog = document.querySelector('#continueDialog');
var muffinAmount = 0;
var selectedMuffin = "";
var products = [];

$('#muffinsDialogClose').on('click', function() {
	muffinsDialog.close();
});

$('#continueDialogClose').on('click', function() {
	continueDialog.close();
});

$('#muffin-cherry').on('click', function() {
	showMuffinsModal("Select quantity", "You've chosen the cherry muffin. How many would you like?", "cherry");
});
$('#muffin-cereal').on('click', function() {
	showMuffinsModal("Select quantity", "You've chosen the cereal muffin. How many would you like?", "cereal");
});
$('#muffin-mango').on('click', function() {
	showMuffinsModal("Select quantity", "You've chosen the mango muffin. How many would you like?", "mango");
});
$('#muffin-cChip').on('click', function() {
	showMuffinsModal("Select quantity", "You've chosen the chocolate chip muffin. How many would you like?", "chocolate chip");
});
$('#muffin-half').on('click', function() {
	showMuffinsModal("Select quantity", "You've chosen the half muffin. How many would you like?", "half");
});
$('#muffin-choco').on('click', function() {
	showMuffinsModal("Select quantity", "You've chosen the chocolate muffin. How many would you like?", "chocolate");
});

$('#muffinsCart').on('click', function() {
	muffinAmount = $('#quantitySelect').children("option:selected").val();
	products.push(selectedMuffin+"["+muffinAmount+"]");
	pushCartToSession(JSON.stringify(products));
	showContinueModal("Added!", "You are ready to checkout! Need to select more items? No problem", null);
});

function pushCartToSession(products) {
	$.post('/api/api.php', {action:"setcart", products: products}).done(function(response) {
		if(response != "") {
			console.log('cart session updated');
		}
	});
}

function showContinueModal(title, content) {
	muffinsDialog.close();
	$('#continueDialogContent').html(content);
	$('#continueDialogTitle').html(title);
	continueDialog.showModal();
}

function showMuffinsModal(title, content, type) {
	if(type != null) selectedMuffin = type;
	$('#muffinsDialogTitle').html(title);
	$('#muffinsDialogContent').html(content);
	muffinsDialog.showModal();
}