<!DOCTYPE html>
<html>
<head>
  <title>Bakers Direct</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="animate.css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
</head>
<body>
<!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title"><img src="logo.png"></span>
      <!-- Add spacer, to align navigation to the right -->
      <div class="mdl-layout-spacer"></div>
      <!-- Navigation. We hide it in small screens. -->
      <nav class="mdl-navigation mdl-layout--large-screen-only">
        <a class="mdl-navigation__link" href="file:///C:/Users/skills/skills2019/muffins.html"  style="font-size: 20px !important;">Muffins</a>
        <a class="mdl-navigation__link" href="file:///C:/Users/skills/skills2019/cakes.html"  style="font-size: 20px !important;">Cakes</a>
        <a class="mdl-navigation__link" href="file:///C:/Users/skills/skills2019/pies.html" style="font-size: 20px !important;" >Pies</a>
        <a class="mdl-navigation__link" href="file:///C:/Users/skills/skills2019/cart.html"  style="font-size: 20px !important;"><i class="material-icons">
shopping_cart
</i></a>
      </nav>
    </div>
  </header>
  <dialog class="mdl-dialog" id="muffinsDialog">
    <h4 class="mdl-dialog__title" id="muffinsDialogTitle"></h4>
    <div class="mdl-dialog__content">
      <p id="muffinsDialogContent">
        
      </p>
      <p>
        Select amount:
        <select id="quantitySelect">
          <option id="quantity1">1</option>
          <option id="quantity2">2</option>
          <option id="quantity3">3</option>
          <option id="quantity4">4</option>
          <option id="quantity5">5</option>
          <option id="quantity6">6</option>
          <option id="quantity7">7</option>
          <option id="quantity8">8</option>
          <option id="quantity9">9</option>
        </select>
      </p>
      <p>
        To continue, click the cart icon!
      </p>
    </div>
    <div class="mdl-dialog__actions">
      <button type="button" id="muffinsCart" class="mdl-button">Add to cart!</button>
      <button type="button" id="muffinsDialogClose" class="mdl-button close">Cancel</button>
    </div>
  </dialog>
  <dialog class="mdl-dialog" id="continueDialog">
    <h4 class="mdl-dialog__title" id="continueDialogTitle"></h4>
    <div class="mdl-dialog__content">
      <p id="continueDialogContent">
        
      </p>
    </div>
    <div class="mdl-dialog__actions">
      <button type="button" id="continueCart" class="mdl-button">Continue</button>
      <button type="button" id="continueDialogClose" class="mdl-button close">Cancel</button>
    </div>
  </dialog>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title" style="width: auto !important; height: auto !important;"><img src="smallLogo.png"></span>
    <nav class="mdl-navigation">
      <a class="mdl-navigation__link" href="">Muffins</a>
      <a class="mdl-navigation__link" href="">Cakes</a>
      <a class="mdl-navigation__link" href="">Pies</a>
      <a class="mdl-navigation__link" href=""><i class="material-icons">
shopping_cart
</i></a>
    </nav>
  </div>
  <main class="mdl-layout__content">


  <h2 style="text-align: center;">Our <span style="color: #0053f7; font-family: 'Oleo Script', cursive;">Muffins</span></h2>

<div class="mdl-grid selection">

  <div class="mdl-cell mdl-cell--4-col selectionItem">
    <img id="selectCake" src="css/cereal.png">
    <hr>
    <h2 style="text-align: center; color: #fe246b; font-family: 'Oleo Script', cursive;">Cereal</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
    <h4>$1.29 <span style="font-size: 20px;">each</span></h4>
    <button id="muffin-cereal" style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
  Add To Bag
</button>
<br>
<br>


  </div>
  <div class="mdl-cell mdl-cell--4-col selectionItem">
    <img id="selectMuffin"  src="css/cherry.png">
      <hr>
    <h2 style="text-align: center; color: #fe246b; font-family: 'Oleo Script', cursive;">Cherry</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
    <h4>$1.09 <span style="font-size: 20px;">each</span></h4>
    <button id="muffin-cherry" style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
Add To Bag
</button>
<br>
<br>
  </div>
  <div class="mdl-cell mdl-cell--4-col selectionItem">
    <img id="selectPie" src="css/mango.png">
      <hr>
    <h2 style="text-align: center; color: #fe246b; font-family: 'Oleo Script', cursive;">Mango</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
    <h4>$1.39 <span style="font-size: 20px;">each</span></h4>
    <button id="muffin-mango" style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
 Add To Bag
</button>
<br>
<br>
  </div>
</div>
<div class="mdl-grid selection">

  <div class="mdl-cell mdl-cell--4-col selectionItem">
    <img id="selectCake" src="css/muffin.png">
    <hr>
    <h2 style="text-align: center; color: #fe246b; font-family: 'Oleo Script', cursive;">Chocolate Chip</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
    <h4>$1.13 <span style="font-size: 20px;">each</span></h4>
    <button id="muffin-cChip" style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
  Add To Bag
</button>
<br>
<br>


  </div>
  <div class="mdl-cell mdl-cell--4-col selectionItem">
    <img id="selectMuffin"  src="css/half.png">
      <hr>
    <h2 style="text-align: center; color: #fe246b; font-family: 'Oleo Script', cursive;">The Half</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
    <h4>$1.29 <span style="font-size: 20px;">each</span></h4>
    <button id="muffin-half" style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
Add To Bag
</button>
<br>
<br>
  </div>
  <div class="mdl-cell mdl-cell--4-col selectionItem">
    <img id="selectPie" src="css/chocolate.png">
      <hr>
    <h2 style="text-align: center; color: #fe246b; font-family: 'Oleo Script', cursive;">Chocolate</h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
    <h4>$1.49 <span style="font-size: 20px;">each</span></h4>
    <button id="muffin-choco" style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
 Add To Bag
</button>
<br>
<br>
  </div>
</div>
<div class="bottomCTA">
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--6-col" style="text-align: center;"><h2>Want<span style="color: #fe246b; font-family: 'Oleo Script', cursive;" > More?</span></h2>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
        <button style="background-color: #0053f7 !important;" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
 Shop Cakes
</button>
  </div>
  <div class="mdl-cell mdl-cell--6-col" style="text-align: center;">
    <img src="css/largeCake.png">

  </div>
</div>

</div>



<footer class="mdl-mega-footer">
  <div class="mdl-mega-footer__middle-section">

    <div class="mdl-mega-footer__drop-down-section">
      <input class="mdl-mega-footer__heading-checkbox" type="checkbox" checked>
      <h1 class="mdl-mega-footer__heading">Contact Us</h1>
      <ul class="mdl-mega-footer__link-list">
        <li><a href="#"><i class="material-icons">
location_on
</i></a>4123 Willow brook lane</li>
        <li><a href="#"><i class="material-icons">
settings_phone
</i></a>610-555-4325</li>
        <li><a href="#"><i class="material-icons">
email
</i></a>info@bakersdirect.com</li>
        
      </ul>
    </div>

    <div class="mdl-mega-footer__drop-down-section">
      <input class="mdl-mega-footer__heading-checkbox" type="checkbox" checked>
      <h1 class="mdl-mega-footer__heading">Products</h1>
      <ul class="mdl-mega-footer__link-list">
        <li><a href="#">Muffins</a></li>
        <li><a href="#">Cakes</a></li>
        <li><a href="#">Pies</a></li>
      </ul>
    </div>

    <div class="mdl-mega-footer__drop-down-section">
      <input class="mdl-mega-footer__heading-checkbox" type="checkbox" checked>
      <h1 class="mdl-mega-footer__heading">Need Help </h1>
      <ul class="mdl-mega-footer__link-list">
        <li><a href="#">Contact us anytime at info@bakersdirect.com</a></li>
      </ul>
    </div>



  </div>



</footer>

      </div>

  </main>

</div>
</body>
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/script.js"></script>

</html>