<?php  
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>test</title>
</head>
<body>
	<input type="text" id="username" placeholder="Username"/>
	<input type="password" id="password" placeholder="Password"/>
	<button id="login">Login</button><br/><br/><br/><br/>
	<input type="text" id="regusername" placeholder="Username"/>
	<input type="password" id="regpassword" placeholder="Password"/>
	<button id="register">Register</button><br/><br/><br/><br/><br/><br/><br/><br/>
	<button id="checkout">Checkout Testing</button>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script type="text/javascript">

		var products = [];

		var handler = StripeCheckout.configure({
			key: 'pk_test_4NydDpwAHwKaXCg9Dxgx8M2Z',
			image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
			locale: 'auto',
			token: function(token) {
				$.post('/api/api.php', {products: JSON.stringify(products), stripeToken: token.id, stripeEmail: token.email}).done(function(response) {
					if(response != "") {

					}
				});
			}
		});

		$('#login').on('click', function() {
			var username = $('#username').val();
			var password = $('#password').val();
			$.post("/api/api.php", {action:"login", username:username, password:password}).done(function(response) {
				if(response != "") {
					console.log(response);
				}
			});
		});

		$('#register').on('click', function() {
			var username = $('#regusername').val();
			var password = $('#regpassword').val();
			$.post("/api/api.php", {action:"register", username:username, password:password}).done(function(response) {
				if(response != "") {
					console.log(response);
				}
			});
		});

		$('#checkout').on('click', function() {
			handler.open({
				name: "Bakers Direct",
				description: "",
				amount: priceOf(products)
			});
		});

		function priceOf(products) {
			if(products !== undefined && products.length > 0) {
				var price = 0;
				$.post('/api/api.php', {action: "getpricing", products: JSON.stringify(products)}).done(function(response) {
					if(response != "") {
						var resp = JSON.parse(response);
						if(resp.result == "priceok") {
							price = resp.price;
						} else {
							price = 0;
						}
					}
				});
				if(price > 0) {
					return price;
				} else {
					return -1;
				}
			}
		}

		window.addEventListener('popstate', function() {
			handler.close();
		})
	</script>
</body>
</html>